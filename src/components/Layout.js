import React,{Component} from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import AuthRoute from './Route/AuthRoute'
import NoAuthRoute from './Route/NoAuthRoute'
import Navbar from './Navbar'
import Landing from './Landing'
import Login from './User/Login'
import Payments from './Payment/Payments'
import Payment from './Payment/Payment'
import AddPayment from './Payment/AddPayment'
import { getBalance } from './User/UserFunctions'

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      balance: 0,
    };
    this.setBalance = this.setBalance.bind(this);
  }
  componentDidMount() {
    if(localStorage.usertoken){
      getBalance().then(res => {
        this.setState({
            balance: res,
        })
      })
    }
  }
  setBalance = (balance) => {this.setState({ balance: balance })}

  render() {
    const LoginWithProps = (props) => {
        return (
          <Login
            setBalance={this.setBalance}
            {...props}
          />
        );
      }
    return (
      <Router>
        <div className="App">
          <Navbar balance={this.state.balance} />
          <Route exact path="/" component={Landing}/>
          <NoAuthRoute exact path="/login" component={LoginWithProps} redirectTo="/"/>
          <AuthRoute exact path="/payment" component={Payments} />
          <AuthRoute setBalance={this.setBalance} path='/add/payment' component={AddPayment} redirectTo="/login" />
          <AuthRoute exact path="/payment/:id" component={Payment} />
        </div>
      </Router>
    );
  }
}

export default Layout
