import axios from 'axios'


export const getPayments = () => {
    return axios
        .get('http://payment-api.artashes.hovesyan.pro/api/payment', {
            headers: { Authorization: `Bearer ${localStorage.usertoken}` }
        })
        .then(response => {
            return response.data
        })
        .catch(err => {
            console.log(err)
        })
}

export const getPayment = (params) => {
    return axios
        .get('http://payment-api.artashes.hovesyan.pro/api/payment/'+params.id, {
            headers: { Authorization: `Bearer ${localStorage.usertoken}` }
        })
        .then(response => {
            return response.data
        })
        .catch(err => {
            console.log(err)
        })
}

export const addPayment = newPayment => {
  return axios
  .post('http://payment-api.artashes.hovesyan.pro/api/payment', newPayment, {
        headers: { 'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.usertoken}`
                  }
    })
    .then(response => {
        return response;
    })
    .catch(err => {
        console.log(err);
        return err
    })
}
