import React, { Component } from 'react'
import { addPayment } from './PaymentFunctions'

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            comment: '',
            amount: '',
            recipient_wallet: '',
            nameError: '',
            commentError: '',
            amountError: '',
            recipient_walletError: '',
            balanceError: ''
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
        console.log(this.props);
    }

    onChange (e) {
        this.setState({ [e.target.name]: e.target.value })
    }
    onSubmit (e) {
        e.preventDefault()
        let isValid = true;
        this.setState({error : {
          name: '',
          comment: '',
          amount: '',
          recipient_wallet: '',
        }})
        if(this.state.name.length < 1){
          this.setState({ nameError : 'Name field is required'})
          isValid = false;
        }
        if(this.state.amount.length < 1){
          this.setState({ amountError : 'Amount field is required'})
          isValid = false;
        }
        if(this.state.recipient_wallet.length < 1){
          this.setState({recipient_walletError : 'Recipient Wallet field is required'})
          isValid = false;
        }
        this.setState({ errors: this.state.errors });
        if(isValid){
          const newPayment = {
              name: this.state.name,
              comment: this.state.comment,
              amount: this.state.amount,
              recipient_wallet: this.state.recipient_wallet
          }

          addPayment(newPayment).then(res => {
            if(res.response){
              this.setState({ balanceError: res.response.data });
            }
            else if (res) {
              this.props.setBalance(res.data.balance);
              this.props.history.push(`/payment`)
            }
          })
        };
    }

    render () {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-4 mt-5 mx-auto">
                    <h2 className="mb-4">Make a Payment</h2>
                        <form noValidate onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <input
                                    type="text"
                                    className={"form-control " + (this.state.nameError ? 'error-input' : '')}
                                    name="name"
                                    placeholder="Name"
                                    value={this.state.name}
                                    onChange={this.onChange}
                                />
                                <span className="error-msg">{this.state.nameError}</span>
                            </div>
                            <div className="form-group">
                                <textarea
                                    rows="3"
                                    className="form-control"
                                    name="comment"
                                    placeholder="Add Coments"
                                    value={this.state.comment}
                                    onChange={this.onChange}
                                />
                                <span className="error-msg">{this.state.commentError}</span>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                  <div className="input-group-prepend">
                                    <span className="input-group-text">$</span>
                                  </div>
                                  <input
                                      type="number"
                                      className={"form-control " + (this.state.amountError ? 'error-input' : '')}
                                      name="amount"
                                      placeholder="Amount"
                                      value={this.state.amount}
                                      onChange={this.onChange}
                                  />
                                  <div className="input-group-append">
                                    <span className="input-group-text">.00</span>
                                  </div>
                                </div>
                                <span className="error-msg">{this.state.amountError}</span>
                            </div>
                            <div className="form-group">
                                <input
                                    type="text"
                                    className={"form-control " + (this.state.recipient_walletError ? 'error-input' : '')}
                                    name="recipient_wallet"
                                    placeholder="Recipient Wallet"
                                    value={this.state.recipient_wallet}
                                    onChange={this.onChange}
                                />
                                <span className="error-msg">{this.state.recipient_walletError}</span>
                            </div>
                            <button type="submit"
                                className="btn btn-lg btn-primary btn-block mb-3">
                                Add
                            </button>
                            <span className="balance-error mb-3">{this.state.balanceError}</span>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Register
