import React, { Component } from 'react'
import { getPayment } from './PaymentFunctions'

class Payments extends Component {
  constructor() {
        super()
        this.state = {
            payment: {},
            loaded: false
        }
    }

    componentDidMount() {
      let id = this.props.match.params;
        getPayment(id).then(res => {
            this.setState({
                payment: res,
                loaded: true
            })
            this.render();
        })
    }

    render() {
      if(this.state.loaded){
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-8 mt-5 mx-auto">
                      <table className="table">
                        <tbody>
                          <tr>
                            <td>Name:</td>
                            <td>{this.state.payment.name}</td>
                          </tr>
                          <tr>
                            <td>Comment:</td>
                            <td>{this.state.payment.comment}</td>
                          </tr>
                          <tr>
                            <td>Status:</td>
                            <td>{this.state.payment.status}</td>
                          </tr>
                          <tr>
                            <td>Amount:</td>
                            <td>{this.state.payment.amount}</td>
                          </tr>
                          <tr>
                            <td>Recipient Wallet:</td>
                            <td>{this.state.payment.recipient_wallet}</td>
                          </tr>
                          <tr>
                            <td>Date:</td>
                            <td>{this.state.payment.created_at}</td>
                          </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
      }
      return ''
    }
}

export default Payments
