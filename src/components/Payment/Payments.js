import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import { getPayments } from './PaymentFunctions'

class Payments extends Component {
  constructor() {
        super()
        this.state = {
            payments: [],
            sortedBy : ''
        }
        this.sortByAmount = this.sortByAmount.bind(this)
        this.sortByDate = this.sortByDate.bind(this)
        this.sortByName = this.sortByName.bind(this)
        if(!localStorage.usertoken){
          this.props.history.push(`/login`)
        }
    }

    componentDidMount() {
        getPayments().then(res => {
            this.setState({
                payments: res,
            })
        })
    }


    sortByAmount(){
      if(this.state.sortedBy === 'amount'){
        this.setState({
            payments: this.state.payments.sort((a, b) => parseInt(b.amount) - parseInt(a.amount)),
            sortedBy : 'amountReverse'
        })
      }
      else {
        this.setState({
            payments: this.state.payments.sort((a, b) => parseInt(a.amount) - parseInt(b.amount)),
            sortedBy : 'amount'
        })
      }
    }

    sortByDate(){

      if(this.state.sortedBy === 'date'){
        this.setState({
            payments: this.state.payments.sort((a, b) => Date.parse(b.created_at) - Date.parse(a.created_at)),
            sortedBy : 'deteReverse'
        })
      }
      else {
        this.setState({
            payments: this.state.payments.sort((a, b) => Date.parse(a.created_at) - Date.parse(b.created_at)),
            sortedBy : 'date'
        })
      }
    }

    sortByName(){
      if(this.state.sortedBy === 'name'){
        this.setState({
            payments: this.state.payments.sort((a,b) => (b.name > a.name) ? 1 : ((a.name > b.name) ? -1 : 0)),
            sortedBy : 'nameReverse'
        })
      }
      else {
        this.setState({
            payments: this.state.payments.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0)),
            sortedBy : 'name'
        })
      }
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-8 mt-5 mx-auto">
                    {!this.state.payments.length ? <h2 className="mb-4">No Payments</h2> :
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col" onClick={this.sortByName}>
                              <span className="sort-btn">Name</span>{this.state.sortedBy === 'name' ?
                               <i className="fa fa-long-arrow-down"></i> : this.state.sortedBy === 'nameReverse' ?
                               <i className="fa fa-long-arrow-up"></i> : '' }
                            </th>
                            <th scope="col" onClick={this.sortByAmount}>
                              <span className="sort-btn">Amount</span>{this.state.sortedBy === 'amount' ?
                               <i className="fa fa-long-arrow-down"></i> : this.state.sortedBy === 'amountReverse' ?
                               <i className="fa fa-long-arrow-up"></i> : '' }
                            </th>
                            <th scope="col" onClick={this.sortByDate}>
                              <span className="sort-btn">Date</span>{this.state.sortedBy === 'date' ?
                               <i className="fa fa-long-arrow-down"></i> : this.state.sortedBy === 'deteReverse' ?
                               <i className="fa fa-long-arrow-up"></i> : '' }
                            </th>
                            <th scope="col">Wallet</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                        {
                          this.state.payments.map((payment,index) => <tr key={index}>
                            <th scope="row">{index+1}</th>
                            <td>{payment.name}</td>
                            <td>{payment.amount}</td>
                            <td>{payment.created_at}</td>
                            <td>{payment.recipient_wallet}</td>
                            <td><Link to={'/payment/' + payment.id}>Details</Link></td>
                          </tr>)
                        }
                        </tbody>
                        </table>
                      }
                    </div>
                </div>
                <div className="row">
                  <div className="mx-auto">
                    <Link to='/add/payment'>
                      <button type="button" className="btn btn-info mb-4">Add Payment</button>
                    </Link>
                  </div>
                </div>
            </div>
        )
    }
}

export default Payments
