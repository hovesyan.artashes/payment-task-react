import React, { Component } from 'react'
import { login,getBalance } from './UserFunctions'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            error: ''
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }
    onSubmit(e) {
        this.setState({ error: '' })
        e.preventDefault()

        const user = {
            email: this.state.email,
            password: this.state.password
        }

        login(user).then(res => {
          if(res.response){
              this.setState({ error: res.response.data.error })
          }
          else if (res) {
            getBalance().then(res => {
              this.props.setBalance(res);
            })
            this.props.history.push(`/payment`)
          }
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-4 mt-5 mx-auto">
                        <form noValidate onSubmit={this.onSubmit}>
                            <h1 className="h3 mb-3 font-weight-normal">
                                Please sign in
                            </h1>
                            <div className="form-group">
                                <label htmlFor="email">Email address</label>
                                <input
                                    type="email"
                                    className="form-control"
                                    name="email"
                                    placeholder="Enter email"
                                    value={this.state.email}
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    name="password"
                                    placeholder="Password"
                                    value={this.state.password}
                                    onChange={this.onChange}
                                />
                            </div>
                            <button
                                type="submit"
                                className="btn btn-lg btn-primary btn-block mb-3"
                            >
                                Sign in
                            </button>
                            <span className="balance-error mb-3">{this.state.error}</span>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login
