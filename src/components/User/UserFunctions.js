import axios from 'axios'

export const register = newUser => {
    return axios
        .post('http://payment-api.artashes.hovesyan.pro/api/register', newUser, {
            headers: { 'Content-Type': 'application/json' }
        })
        .then(response => {
            return response
        })
        .catch(err => {
            console.log(err)
        })
}

export const login = user => {
    return axios
        .post(
            'http://payment-api.artashes.hovesyan.pro/api/login',
            {
                email: user.email,
                password: user.password
            },
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        .then(response => {
            localStorage.setItem('usertoken', response.data.token)
            return response.data.token
        })
        .catch(err => {
            return err
        })
}

export const getBalance = () => {
    return axios
        .get('http://payment-api.artashes.hovesyan.pro/api/balance', {
            headers: { Authorization: `Bearer ${localStorage.usertoken}` }
        })
        .then(response => {
            return response.data
        })
        .catch(err => {
            console.log(err)
        })
}
