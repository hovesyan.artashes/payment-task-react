import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import Layout from './components/Layout'

function App() {
  return (
    <Layout/>
  );
}

export default App;
