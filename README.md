Single Page application build in React js with using Rest API.
Application have access levels:
  1. for unauthorized is available only homepage and login
  2. For authorized user is available payments page where he sees his payments, payment inner page, and add payment.
Add payment form has client side and server side validation
User Authorization implemented using JWT

Test users:
  1. email: test@test.com
     password: qwerty

  2. email: test2@test.com
     password: qwerty

  3. email: test3@test.com
     password: qwerty

Each user has his own balance

To start application use commands "npm install" then "npm start"
